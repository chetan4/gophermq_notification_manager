module GMQ
	Configurations = ::OpenStruct.new

	def self.init		
		set_environment
		
		if ENV["GMQ_ENV"] == "development"
			GMQ::Configurations.gophermq_server_uri = "http://localhost:3000/"
		elsif ENV["GMQ_ENV"] == "staging"
			GMQ::Configurations.gophermq_server_uri = "http://staging.gophermq.com/"
		elsif ENV["GMQ_ENV"] == "production"
			GMQ::Configurations.gophermq_server_uri = "http://gophermq.com/"
		end
	end

	def self.set_environment
		env_arguments = ARGV[0]
		
		if !env_arguments.nil? && !env_arguments.empty?
			key, value = env_arguments.split("=")
			ENV["GMQ_ENV"] = value if key.strip == "GMQ_ENV"
			return value
		end
		
		ENV["GMQ_ENV"] = "development"		
	end

	module ConfigurationHelpers
		def config
			GMQ::Configurations
		end
	end
end