require "rubygems"
require "bundler/setup"
require "eventmachine"
require "em-hiredis"
require "em-http-request"
require "logger"
require "json"
require "ostruct"
require "pry"
require "em-websocket"

require File.expand_path("../../config/init.rb", __FILE__)
require File.expand_path("../gmq/notification_manager.rb", __FILE__)
require File.expand_path("../gmq/subscription_manager.rb", __FILE__)
LOGFILE = File.expand_path("../../logs/gmq.log", __FILE__)

module GMQ	
	EM.run do
		GMQ.init
		redis 		= EM::Hiredis.connect
		GMQ::Logger	= ::Logger.new(LOGFILE)		
		GMQ::NotificationManager.start(redis)	
		GMQ::SubscriptionManager.start(redis)		
	end
end
