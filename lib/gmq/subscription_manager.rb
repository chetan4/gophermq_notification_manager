module GMQ
	class SubscriptionManager
		include GMQ::ConfigurationHelpers

		attr_accessor :redis, :subscribers

		def self.start(redis)
			@sm = SubscriptionManager.new(redis)
			@sm.watch_subscription_notifications
		end

		def initialize(redis)
			@redis 		 = redis
			@subscribers = {}			
		end

		def watch_subscription_notifications
			EM::WebSocket.run(:host => "127.0.0.1", :port => 8799) do |ws|
				key 			 = nil
				subscription_key = nil

				ws.onopen {|handshake|						
					query_string = handshake.query.map{|k,v| "#{k}=#{v}"}.join("&")				
					#key, subscription_key = generate_key_from_path(handshake.path + "?" + query_string, ws)
					handshake_path = handshake.path + "?" + query_string

					ws.close if (/\/projects\/.{20}\/queues\/.*/ =~ handshake_path.to_s) == -1			
					http = EM::HttpRequest.new(config.gophermq_server_uri + "/authenticate").post(:body => {:url => handshake_path })
					http.callback{ |status|	
						ws.close if http.response_header.status != 200									
						GMQ::Logger.info("Callback Status: #{http.response_header.status}")
						key				= "gophermq:"
						path_params 	= handshake_path.split("/")			
						key 			+= path_params[2]			
						key  			+= "_" + path_params[4]
						subscription_key =  subscription_key(handshake.path)

						GMQ::Logger.info("[HANDSHAKE]: Done.")
						check = redis.exists(key)
						check.callback{|status| 
							subscribe_to_queue(key, subscription_key, ws)
							GMQ::Logger.info("[SUBSCRIPTIONS] #{@subscribers.keys.inspect}")
						} 
					
						check.errback{ |status|
							GMQ::Logger.info("#{key} SUBSCRIPTION QUEUE DOES NOT EXIST")
							ws.close
						}
					}
					http.errback{ |status|
						GMQ::Logger.info("Errback Status: #{http.response_header.status}")
						GMQ::Logger.info("Authentication failed.")
						ws.close
					}					
				}

				ws.onclose { 					
					unsubscribe_from_queue(key, subscription_key, ws)
					GMQ::Logger.info("[CLOSING Socket]")					
				}
			end
		end

		def subscribe_to_queue(key, subscription_key, ws)			
			application_id, queue_name = appid_and_queuename(subscription_key)

			http = EM::HttpRequest.new(config.gophermq_server_uri + "/projects/#{application_id}/queues/#{queue_name}/subscribe").put(:body => "")			
			http.callback{|status| GMQ::Logger.info "CALLBACK: Subscribe status: #{http.response_header.status}"}
			http.errback{|status| GMQ::Logger.info "ERRBACK: Subscribe status: #{http.response_header.status}"}

			if @subscribers.has_key?(key)
				@subscribers[key.to_s].push(ws)
			else
				@subscribers[key] = []
				@subscribers[key.to_s].push(ws)

				GMQ::Logger.info("[SUBSCRIBE] to #{subscription_key}")
				redis.pubsub.subscribe(subscription_key) do |msg|
					GMQ::Logger.info "[SUBSCRIPTION] MESSAGE ARRIVED: #{msg}"
					hash_msg = JSON.parse(msg)
					job  	 = hash_msg["job"]
					GMQ::Logger.info "[SUBSCRIPTION] JOB: #{job}"
					@subscribers[key.to_s].each {|sock| sock.send(job) }				
				end
			end
		end

		def unsubscribe_from_queue(key, subscription_key, ws)			
			@subscribers.each do |key, subscriber_sockets|				
				if subscriber_sockets.include?(ws)
					subscriber_sockets.delete(ws)
					
					# Change status of queue if every one has unsubscribed.
					if @subscribers[key.to_s].empty?
						@subscribers.delete(key.to_s)
						application_id, queue_name = appid_and_queuename(subscription_key)					

						GMQ::Logger.info "[UNSUBSCRIBE]: #{subscription_key}"
						http = EM::HttpRequest.new(config.gophermq_server_uri + "/projects/#{application_id}/queues/#{queue_name}/unsubscribe").put(:body => "")			
						http.callback{|status| GMQ::Logger.info "CALLBACK: UnSubscribe status: #{http.response_header.status}"}
						http.errback{|status| GMQ::Logger.info "ERRBACK: UnSubscribe status: #{http.response_header.status}"}
					end
				end
			end
		end
		
		def subscription_key(params)
			split_params = params.split("/")
			"gophermq_subscription_" + split_params[2].to_s + "_" + split_params[4].to_s
		end	

		def appid_and_queuename(subscription_key)
			params = subscription_key.split("_")
			[params[2], params[3]]
		end	
	end

	
end