module GMQ	
	class NotificationManager				
		include GMQ::ConfigurationHelpers

		attr_accessor :redis

		def self.start(redis)					
			@nm = NotificationManager.new(redis)			
			@nm.watch_notifications
			@nm.start_retry_attempts
		end

		def initialize(redis)
			@redis = redis
		end

		def notify_user(notification_uri, jobs)
			EM::HttpRequest.new(notification_uri).post(:body => {:jobs => jobs }.to_json)
		end

		def watch_notifications			
			@redis.pubsub.subscribe("gophermq_notifiable_notification") do |msg|
				puts "Message arrived #{msg}"
				notification_hash = JSON.parse(msg)
				notification_uri  = notification_hash["notify_uri"]
				key 			  = notification_hash["key"]

				@redis.lrange(key, 0, -1).callback {|jobs|
					puts "JObs #{jobs}"						
					http = notify_user(notification_uri, jobs)
					http.errback { 
						GMQ::Logger.info "Redis Job Failed..."
					}
					http.callback{
						GMQ::Logger.info "[CALLBACK:] #{http.response_header.status}"
						if http.response_header.status != 200
							retry_job = {:jobs => jobs, :notify_uri => notification_uri}
							EM.add_timer(10){
								retry_later(retry_job)
							}
						end
					}
				}.errback{|msg| GMQ::Logger.info "[ERRBACK]: Watch Notification Failure #{msg}"}
			end						
		end

		def retry_later(job)			
			@redis.lpush("gophermq_retry_notification", job.to_json)
		end

		def start_retry_attempts								
			q = @redis.rpop("gophermq_retry_notification") 
			q.callback {|job| 
				if !job.nil?
					puts "Job: #{job}"
					retry_job 		 = JSON.parse(job)
					notification_uri = retry_job["notify_uri"]
					job 			 = retry_job["job"]

					notify_user(notification_uri, job)
				end
				EM.next_tick { start_retry_attempts }
			}
			q.errback {
				EM.next_tick { start_retry_attempts }
			}
			
		end

		def watch_subscriptions
			EM.defer{
				@redis.pubsub.subscribe("gophermq_subscription_notification") do |msg|
					puts "Message for subscription #{msg}"
				end
			}
		end
	end
end